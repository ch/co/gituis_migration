#!/bin/bash

set -e

if [ "$#" -ne 3 ]; then
 echo "usage: $0 [GIT_UIS_ACCOUNT] [SOURCEREPO] [DESTINATION]"
 echo "e.g. to move ch-co@git.uis.cam.ac.uk:example/repo to https://gitlab.developers.cam.ac.uk/ch/co/repo, run:"
 echo
 echo "$0 ch-co example/repo ch/co/repo"
 exit
fi

if [ -z "$UIS_GITLAB_TOKEN" ]; then
  echo UIS_GITLAB_TOKEN not set. You need to e.g. \"export UIS_GITLAB_TOKEN=mySecretToken\"
  echo If necessary, go to https://gitlab.developers.cam.ac.uk/profile/personal_access_tokens and
  echo create a token with api scope.
  exit 1
fi

if [ ! -x $(which jq) ] ; then
  echo no jq executable found
  exit 2
fi

GIT_UIS_ACCOUNT=$1
SOURCEREPO=$2
DESTINATION=$3

DESTREPO=$(basename $DESTINATION)
# will return an error in $SOURCEREPO does not exist and thus 
# cause script to bail due to 
ssh $GIT_UIS_ACCOUNT@git.uis.cam.ac.uk desc $SOURCEREPO >/dev/null || (echo "Error: does $SOURCEREPO exist?"; exit 1)

REDIRECT_INFO=$(ssh $GIT_UIS_ACCOUNT@git.uis.cam.ac.uk to-gitlab $SOURCEREPO | grep "^redirect to" ; true)

if [ -n "$REDIRECT_INFO" ] ; then
  echo $SOURCEREPO is already set to $REDIRECT_INFO
  exit
fi

DESCRIPTION=$(ssh $GIT_UIS_ACCOUNT@git.uis.cam.ac.uk desc $SOURCEREPO)

DESTGROUP=$(dirname $DESTINATION)
DESTREPO=$(basename $DESTREPO)

ENC_DESTGROUP=$(echo $DESTGROUP | perl -MURI::Escape -ne 'chomp;print uri_escape($_)')
DESTGROUPINFO=$(curl -s --header "PRIVATE-TOKEN: $UIS_GITLAB_TOKEN" "https://gitlab.developers.cam.ac.uk/api/v4/groups/$ENC_DESTGROUP")

DESTGROUPID=$(echo $DESTGROUPINFO | jq ".id")

if [ -z "$DESTGROUPID" ] || [ "$DESTGROUPID" == "null" ] ; then
  echo could not determine gitlab group ID for $DESTGROUP
  exit 1
fi

DESTGROUPVISIBILITY=$(echo $DESTGROUPINFO | jq -r .visibility)

echo "will migrate $SOURCEREPO to $DESTGROUP/$DESTREPO (group id $DESTGROUPID) with visibility $DESTGROUPVISIBILITY"

NEWPROJECTURL="https://gitlab.developers.cam.ac.uk/api/v4/projects?name=$DESTREPO&namespace_id=$DESTGROUPID&visibility=$DESTGROUPVISIBILITY"

if [ -n "$DESCRIPTION" ] ; then
  ENC_DESCRIPTION=$(echo $DESCRIPTION | perl -MURI::Escape -ne 'chomp;print uri_escape($_)')
  NEWPROJECTURL="$NEWPROJECTURL&description=$ENC_DESCRIPTION"
fi

NEWPROJECT=$(curl -s --header "PRIVATE-TOKEN: $UIS_GITLAB_TOKEN" -X POST $NEWPROJECTURL)

URL=$(echo $NEWPROJECT | jq ".http_url_to_repo" | tr -d '"')
if [ -z "$URL" ] || [ "$URL" == "null" ] ; then
  echo $NEWPROJECT | jq "."
  echo problem creating project $DESTREPO
  exit 1
fi

echo "created $URL"

WORKDIR=$(mktemp -d)

git clone $GIT_UIS_ACCOUNT@git.uis.cam.ac.uk:${SOURCEREPO} $WORKDIR --mirror
pushd $WORKDIR
git remote remove origin
git remote add origin git@gitlab.developers.cam.ac.uk:$DESTGROUP/$DESTREPO
git push -u origin --all
git push -u origin --tags
ssh $GIT_UIS_ACCOUNT@git.uis.cam.ac.uk to-gitlab $SOURCEREPO $URL
popd

[ -d "$WORKDIR" ] && rm -rf "$WORKDIR"
