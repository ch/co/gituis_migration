Requirements
============

jq (command-line json querier)

perl with URI::Escape (e.g. apt-get install liburi-perl)

Usage instructions
==================
You need to export your token for gitlab.developers.cam.ac.uk:

$ export UIS_GITLAB_TOKEN=myGitlabToken

If you do not have one, go to

https://gitlab.developers.cam.ac.uk/profile/personal_access_tokens 

and create a token with the "api" scope. (Treat your personal access token with
the same respect you would give to your Raven password!)

Assuming that the destination (sub)group mydept/mygroup exists on gitlab (if it
does not the script will give an error), and you want to migrate:
~~~~
mydept-mygroup@git.uis.cam.ac.uk:foo/sourcerepo 
~~~~
to a repository named destrepo under the mydept/mygroup subgroup, i.e.
~~~~
git@gitlab.developers.cam.ac.uk:mydept/mygroup/destrepo
https://gitlab.developers.cam.ac.uk/mydept/mygroup/destrepo
~~~~
then you should run
~~~~
./migrate-to-gitlab.sh mydept-mygroup foo/sourcerepo mydept/mygroup/destrepo
~~~~
NB this will also leave behind a redirect on git.uis.cam.ac.uk, per

https://git.uis.cam.ac.uk/gitlab.html

Repository permissions/visibility
=================================
You may want to carefully check the permissions on the newly-created repository
on gitlab: no attempt is made to migrate permissions from git.uis.cam.ac.uk,
and the new gitlab repository will inherit whatever default permissions apply
to the (sub)group it is located within. Similarly, the project visibility will
be that of its (sub)group.

Report status of reps on git.uis
================================

Run e.g.
~~~~
ssh acct@git.uis.cam.ac.uk ls-gitlab
~~~~
